project-init: cp-env build down up npm-i

cp-env: cp-laravel-env cp-laravel-env-testing

npm-i:
	./npm i

build:
	docker-compose -f docker-compose.local.yml build

up:
	docker-compose -f docker-compose.local.yml up -d

down:
	docker-compose -f docker-compose.local.yml down

composer-i:
	docker-compose -f docker-compose.local.yml exec app composer install

db-migrate:
	docker-compose -f docker-compose.local.yml exec app php artisan migrate

db-fresh:
	docker-compose -f docker-compose.local.yml exec app php artisan migrate:fresh

db-seed:
	docker-compose -f docker-compose.local.yml exec app php artisan db:seed

storage-link:
	docker-compose -f docker-compose.local.yml exec app php artisan storage:link

gen-key:
	docker-compose -f docker-compose.local.yml exec app php artisan key:generate

passport-install:
	docker-compose -f docker-compose.local.yml exec app php artisan passport:install

tests:
	docker-compose -f docker-compose.local.yml exec app php artisan test

gen-swagger:
	docker-compose -f docker-compose.local.yml exec app php artisan l5-swagger:generate

cp-laravel-env:
	cp src/.env.example src/.env

cp-laravel-env-testing:
	cp src/.env.testing.example src/.env.testing
