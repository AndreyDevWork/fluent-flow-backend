#!/usr/bin/env bash

cp ./.env.testing.example ./.env.testing

chmod 777 -R /var/www/bootstrap/cache
chmod 777 -R /var/www/storage

composer install
chmod 777 -R /var/www/vendor

echo php artisan migrate
php artisan migrate

echo artisan db:seed
php artisan db:seed

echo artisan storage:link
php artisan storage:link

echo artisan l5-swagger:generate
php artisan l5-swagger:generate

php artisan cache:clear
php artisan config:clear
php artisan route:clear
php artisan passport:keys || true
#php artisan horizon

php-fpm
