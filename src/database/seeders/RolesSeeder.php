<?php

namespace Database\Seeders;

use App\Enums\Roles;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::firstOrCreate(["role" => Roles::STUDENT->value]);
        Role::firstOrCreate(["role" => Roles::SPEAKER->value]);
    }
}
