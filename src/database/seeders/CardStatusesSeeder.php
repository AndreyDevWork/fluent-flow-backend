<?php

namespace Database\Seeders;

use App\Enums\CardStatuses;
use App\Models\CardStatus;
use Illuminate\Database\Seeder;

class CardStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CardStatus::firstOrCreate(["status" => CardStatuses::DRAFT->value]);
        CardStatus::firstOrCreate(["status" => CardStatuses::PUBLISHED->value]);
    }
}
