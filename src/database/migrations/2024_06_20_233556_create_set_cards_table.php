<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create("set_cards", function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("set_id");
            $table->unsignedBigInteger("card_id");

            $table->index("set_id", "set_cards_set_idx");
            $table->index("card_id", "set_cards_card_idx");

            $table
                ->foreign("set_id", "post_tag_set_fk")
                ->on("sets")
                ->references("id")
                ->onDelete("cascade");
            $table
                ->foreign("card_id", "post_tag_car_fk")
                ->on("cards")
                ->references("id")
                ->onDelete("cascade");

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists("table_set_cards");
    }
};
