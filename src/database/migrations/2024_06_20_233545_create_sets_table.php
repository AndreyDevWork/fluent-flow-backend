<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create("sets", function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->foreignId("profile_id")->constrained("profiles");
            $table
                ->foreignId(column: "status_id")
                ->constrained(table: "card_statuses");

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists("table_sets");
    }
};
