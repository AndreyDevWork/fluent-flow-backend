<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create("cards", function (Blueprint $table) {
            $table->id();
            $table->string("term")->nullable();
            $table->string("translate")->nullable();
            $table->foreignId("status_id")->constrained("card_statuses");
            $table->foreignId("profile_id")->constrained("profiles");

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists("table_cards");
    }
};
