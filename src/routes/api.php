<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\Authorize\GetAuthorizeAppController;
use App\Http\Controllers\Card\IndexCardController;
use App\Http\Controllers\Card\ShowCardController;
use App\Http\Controllers\Card\StoreCardController;
use App\Http\Controllers\Card\UpdateCardController;
use App\Http\Controllers\Profile\DeleteAvatarController;
use App\Http\Controllers\Profile\IndexProfileController;
use App\Http\Controllers\Profile\ShowProfileController;
use App\Http\Controllers\Profile\StoreProfileController;
use App\Http\Controllers\Profile\UpdateProfileController;
use App\Http\Controllers\Profile\UploadAvatarController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get("/log-test", function () {
    Log::error("This is a test log for Graylog: log level === error");
    return "Log has been sent to Graylog!";
});

Route::group(
    attributes: ["prefix" => "auth"],
    routes: function () {
        Route::post("/register", RegisterController::class);
        Route::post("/send-email-verification-message", [
            VerifyEmailController::class,
            "sendEmailVerificationMessage",
        ])->middleware("auth:api");
        Route::get("email/verify/{id}/{hash}", [
            VerifyEmailController::class,
            "verificationEmail",
        ])->name("verification.verify");
    }
);

Route::group(
    attributes: ["middleware" => ["auth:api", "canUseProfile"]],
    routes: function () {
        Route::get("/profile/{user}", ShowProfileController::class);
        Route::patch(
            "/profile/{user}",
            UpdateProfileController::class
        )->middleware("canUpdateProfile");
        Route::get("/profile", IndexProfileController::class);
        Route::post("/profile/avatar", UploadAvatarController::class);
        Route::delete("/profile/avatar", DeleteAvatarController::class);

        Route::post("/card", StoreCardController::class);
        Route::patch("/card/{card}", UpdateCardController::class);
        Route::get("/card/{card}", ShowCardController::class);
        Route::get("/card", IndexCardController::class);
    }
);

Route::post("/profile", StoreProfileController::class)
    ->middleware("canCreateProfile")
    ->middleware("auth:api");

Route::get("/get-authorize-app", GetAuthorizeAppController::class);
