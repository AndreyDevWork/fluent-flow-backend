<?php

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class ProfilePatchDTO extends DataTransferObject
{
    public readonly ?string $date_of_birth;
    public readonly ?string $firstname;
    public readonly ?string $lastname;
    public readonly ?string $about_me;
    public readonly ?string $email;
}
