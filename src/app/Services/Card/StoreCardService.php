<?php

namespace App\Services\Card;

use App\Models\Card;
use App\Models\Profile;

class StoreCardService
{
    public function store(Profile $profile, array $data): Card
    {
        return $profile->cards()->create($data);
    }
}
