<?php

namespace App\Services\Profile;

use App\DTO\ProfilePatchDTO;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ProfileUpdateService
{
    public function update(ProfilePatchDTO $data, User $user): Profile
    {
        $data->email && $user->update(["email" => $data->email]);
        $user->profile->update(
            array_filter($data->toArray(), fn($value) => $value)
        );

        return $user->profile;
    }
}
