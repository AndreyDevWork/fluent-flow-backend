<?php

namespace App\Services\Profile;

use App\Components\FileSystem\Image\ImageHandlerBuilder\ImageHandlerBuilder;
use App\Components\FileSystem\Image\ImageHandlerBuilder\ImageHandlerManager;
use App\Models\Image;
use App\Models\Profile;
use Illuminate\Http\UploadedFile;

class AvatarService
{
    protected ImageHandlerManager $imageHandlerManager;
    public function __construct()
    {
        $this->imageHandlerManager = new ImageHandlerManager(
            new ImageHandlerBuilder()
        );
    }

    public function uploadAvatar(UploadedFile $avatar, Profile $profile)
    {
        $imagePath = $this->imageHandlerManager
            ->createAvatarHandler(uploadedImage: $avatar)
            ->getLocalImagePath();

        if ($profile->avatar) {
            $this->deleteAvatar(avatar: $profile->avatar);
        }

        return $profile->avatar()->create([
            "url" => $imagePath,
            "imageable_type" => Profile::class,
            "imageable_id" => $profile->id,
        ]);
    }

    public function deleteAvatar(Image $avatar)
    {
        $avatar->deleteFromDisc();
        return $avatar->delete();
    }
}
