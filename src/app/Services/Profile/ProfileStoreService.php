<?php

namespace App\Services\Profile;

use App\Models\Profile;
use App\Traits\HasJsonResponses;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProfileStoreService
{
    public function __construct(protected AvatarService $avatarService)
    {
    }

    public function store($data): Profile
    {
        $profile = Auth::user()->profile()->create($data);

        if (array_key_exists(key: "images", array: $data)) {
            $this->avatarService->uploadAvatar(
                avatar: $data["images"][0],
                profile: $profile
            );
            $profile->refresh();
        }

        return $profile;
    }
}
