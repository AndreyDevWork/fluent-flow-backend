<?php

namespace App\Interfaces\ChainOfResponsibility;

interface Handler
{
    public function handle();
}
