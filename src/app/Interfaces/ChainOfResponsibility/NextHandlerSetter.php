<?php

namespace App\Interfaces\ChainOfResponsibility;

interface NextHandlerSetter
{
    public function setNext(Handler $handler): self;
}
