<?php

namespace App\Components\FileSystem\Image\ImageHandlerBuilder;

use App\Components\FileSystem\Enums\Catalogs;
use App\Components\FileSystem\Image\ImageHandler\ImageHandler;
use Illuminate\Http\UploadedFile;
use Intervention\Image\MediaType;

class ImageHandlerBuilder implements ImageHandlerBuilderInterface
{
    private ImageHandler $imageHandler;

    public function __construct()
    {
        $this->create();
    }

    public function create(): ImageHandlerBuilderInterface
    {
        $this->imageHandler = new ImageHandler();

        return $this;
    }

    public function setUploadedImage(
        UploadedFile $uploadedImage
    ): ImageHandlerBuilderInterface {
        $this->imageHandler->uploadedImage = $uploadedImage;

        return $this;
    }

    public function setCatalog(Catalogs $catalog): ImageHandlerBuilderInterface
    {
        $this->imageHandler->catalog = $catalog;

        return $this;
    }

    public function setScale(
        int $width,
        int $height
    ): ImageHandlerBuilderInterface {
        $this->imageHandler->width = $width;
        $this->imageHandler->height = $height;

        return $this;
    }

    public function setMediaType(
        MediaType $mediaType
    ): ImageHandlerBuilderInterface {
        $this->imageHandler->mediaType = $mediaType;

        return $this;
    }

    public function build(): ImageHandler
    {
        $result = $this->imageHandler;
        $this->create();

        return $result;
    }
}
