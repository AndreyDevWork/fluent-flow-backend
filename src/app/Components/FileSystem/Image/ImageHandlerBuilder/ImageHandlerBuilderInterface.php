<?php

namespace App\Components\FileSystem\Image\ImageHandlerBuilder;

use App\Components\FileSystem\Enums\Catalogs;
use App\Components\FileSystem\Image\ImageHandler\ImageHandler;
use Illuminate\Http\UploadedFile;
use Intervention\Image\MediaType;

interface ImageHandlerBuilderInterface
{
    public function create(): ImageHandlerBuilderInterface;

    public function setCatalog(Catalogs $catalog): ImageHandlerBuilderInterface;

    public function setScale(
        int $width,
        int $height
    ): ImageHandlerBuilderInterface;

    public function setMediaType(
        MediaType $mediaType
    ): ImageHandlerBuilderInterface;

    public function setUploadedImage(
        UploadedFile $uploadedImage
    ): ImageHandlerBuilderInterface;

    public function build(): ImageHandler;
}
