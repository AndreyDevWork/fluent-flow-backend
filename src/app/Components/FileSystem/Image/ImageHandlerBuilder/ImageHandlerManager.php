<?php

namespace App\Components\FileSystem\Image\ImageHandlerBuilder;

use App\Components\FileSystem\Enums\Catalogs;
use App\Components\FileSystem\Image\ImageHandler\ImageHandler;
use Illuminate\Http\UploadedFile;
use Intervention\Image\MediaType;

class ImageHandlerManager
{
    public function __construct(
        protected ImageHandlerBuilderInterface $imageHandlerBuilder
    ) {
    }

    public function createAvatarHandler(
        UploadedFile $uploadedImage
    ): ImageHandler {
        return $this->imageHandlerBuilder
            ->setUploadedImage($uploadedImage)
            ->setMediaType(mediaType: MediaType::IMAGE_WEBP)
            ->setCatalog(catalog: Catalogs::AVATARS)
            ->setScale(width: 2000, height: 2000)
            ->build();
    }
}
