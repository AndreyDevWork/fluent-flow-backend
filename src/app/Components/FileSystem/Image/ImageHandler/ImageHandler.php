<?php

namespace App\Components\FileSystem\Image\ImageHandler;

use App\Components\FileSystem\Enums\Catalogs;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Drivers\Imagick\Driver;
use Intervention\Image\ImageManager;
use Intervention\Image\Interfaces\ImageInterface;
use Intervention\Image\MediaType;

class ImageHandler
{
    use HasImageHandlerConverter;

    public Catalogs $catalog;
    public int $width;
    public int $height;
    public MediaType $mediaType;

    public UploadedFile $uploadedImage;

    protected string $pathToFile;

    protected ImageInterface $image;

    protected function saveImage(): self
    {
        $this->pathToFile = Storage::disk()->put(
            path: generatePathFile(
                firstCatalog: $this->catalog->value,
                now: Carbon::now()
            ),
            contents: $this->uploadedImage
        );

        return $this;
    }

    protected function readImage(): self
    {
        $this->image = (new ImageManager(driver: new Driver()))->read(
            Storage::path($this->pathToFile)
        );

        return $this;
    }

    protected function scaleImage(): self
    {
        if ($this->width && $this->height) {
            $this->image->scale(width: $this->width, height: $this->height);
        }

        return $this;
    }

    protected function convertToFormat(): self
    {
        switch ($this->mediaType) {
            case MediaType::IMAGE_WEBP:
                $this->toWebp();
                break;
            case MediaType::IMAGE_PNG:
                $this->toPng();
                break;
        }

        return $this;
    }

    public function getLocalImagePath(): string
    {
        $this->saveImage()->readImage()->scaleImage()->convertToFormat();

        return $this->pathToFile;
    }
}
