<?php

namespace App\Components\FileSystem\Image\ImageHandler;

use App\Components\FileSystem\Enums\MediaFormats;
use Illuminate\Support\Facades\Storage;

trait HasImageHandlerConverter
{
    protected function toWebp(): void
    {
        $this->image
            ->encodeByMediaType(type: $this->mediaType)
            ->save(
                filepath: Storage::path($this->pathToFile) .
                    MediaFormats::WEBP->value
            );

        $this->pathToFile = $this->pathToFile . MediaFormats::WEBP->value;
    }

    protected function toPng(): void
    {
        $this->image
            ->encodeByMediaType(type: $this->mediaType)
            ->save(
                filepath: Storage::path($this->pathToFile) .
                    MediaFormats::PNG->value
            );

        $this->pathToFile = $this->pathToFile . MediaFormats::PNG->value;
    }
}
