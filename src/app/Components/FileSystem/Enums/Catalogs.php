<?php

namespace App\Components\FileSystem\Enums;

enum Catalogs: string
{
    case AVATARS = "avatars";
    case PHOTOS = "photos";
    case MESSAGES = "messages";
    case CARDS = "cards";
}
