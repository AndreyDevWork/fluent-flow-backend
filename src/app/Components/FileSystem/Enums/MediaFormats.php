<?php

namespace App\Components\FileSystem\Enums;

enum MediaFormats: string
{
    case WEBP = ".webp";
    case PNG = ".png";
}
