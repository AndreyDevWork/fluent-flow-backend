<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Card extends Model
{
    use HasFactory;

    protected $table = "cards";
    protected $fillable = ["term", "translate", "status_id"];

    public function author(): BelongsTo
    {
        return $this->belongsTo(Profile::class, "profile_id", "id");
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(CardStatus::class, "status_id", "id");
    }
}
