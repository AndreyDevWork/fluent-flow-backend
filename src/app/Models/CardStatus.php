<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardStatus extends Model
{
    protected $table = "card_statuses";
}
