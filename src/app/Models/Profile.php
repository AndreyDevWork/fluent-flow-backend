<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Profile extends Model
{
    use HasFactory;

    protected $table = "profiles";
    protected $fillable = [
        "date_of_birth",
        "firstname",
        "lastname",
        "user_id",
        "role_id",
        "about_me",
    ];
    protected $with = ["role", "user", "avatar"];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public function avatar(): MorphOne
    {
        return $this->morphOne(Image::class, "imageable");
    }

    public function cards(): HasMany
    {
        return $this->hasMany(Card::class);
    }
}
