<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $guarded = false;

    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }

    public function deleteFromDisc()
    {
        $webpPath = str_replace("storage/", "", $this->url);

        Storage::disk()->delete(str_replace("storage/", "", $this->url));
        Storage::disk()->delete(str_replace(".webp", "", $webpPath));
    }
}
