<?php

function minute(): int
{
    return 60;
}

function hour(): int
{
    return minute() * 60;
}

function day(): int
{
    return hour() * 24;
}
