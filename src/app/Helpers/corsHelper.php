<?php

function ddh(mixed ...$vars)
{
    header(header: "Access-Control-Allow-Origin: *");
    header(header: "Access-Control-Allow-Methods: *");
    header(header: "Access-Control-Allow-Headers: *");

    dd(...func_get_args());
}

function dumph(mixed ...$vars)
{
    header(header: "Access-Control-Allow-Origin: *");
    header(header: "Access-Control-Allow-Methods: *");
    header(header: "Access-Control-Allow-Headers: *");

    dump(...func_get_args());
}
