<?php

use Carbon\Carbon;

function generatePathFile(string $firstCatalog, Carbon $now): string
{
    return $path =
        "$firstCatalog/" . $now->year . "/" . $now->month . "/" . $now->day;
}
