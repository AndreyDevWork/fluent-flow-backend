<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait HasJsonResponses
{
    function invalidGrantTypeResponse(): JsonResponse
    {
        return response()->json(
            data: ["message" => "Invalid grant type"],
            status: \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST
        );
    }

    function invalidLoginOrPasswordResponse(): JsonResponse
    {
        return response()->json(
            data: [
                "message" => "Invalid username or password",
            ],
            status: Response::HTTP_UNAUTHORIZED
        );
    }
    function accessDeniedResponse(): JsonResponse
    {
        return response()->json(
            data: [
                "message" => "You have not permission to access this request",
            ],
            status: Response::HTTP_FORBIDDEN
        );
    }

    function profileAlreadyExists(): JsonResponse
    {
        return response()->json(
            data: [
                "message" => "Profile already exists for this user.",
            ],
            status: Response::HTTP_CONFLICT
        );
    }

    public function resourceNotFoundResponse(): JsonResponse
    {
        return response()->json(
            data: [
                "message" => "Resource not found",
            ],
            status: Response::HTTP_NOT_FOUND
        );
    }
}
