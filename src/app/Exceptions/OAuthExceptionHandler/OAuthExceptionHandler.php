<?php

namespace App\Exceptions\OAuthExceptionHandler;

use App\Traits\HasJsonResponses;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class OAuthExceptionHandler
{
    use HasJsonResponses;

    protected $grantTypes = ["refresh_token", "password"];
    protected $response;
    protected $request;
    protected $exception;

    public function __construct(Request $request, $exception)
    {
        $this->request = $request;
        $this->exception = $exception;
    }

    public function handleGrantType()
    {
        if (!in_array($this->request->grant_type, $this->grantTypes)) {
            $this->response = $this->invalidGrantTypeResponse();
        }

        return $this;
    }

    public function handleUser()
    {
        $user = User::where("username", $this->request->username)->first();
        if (!$user || !Hash::check($this->request->password, $user->password)) {
            $this->response = $this->invalidLoginOrPasswordResponse();
        }

        return $this;
    }

    public function renderException()
    {
        if (!$this->response) {
            return $this->exception->render($this->request);
        }

        return $this->response;
    }
}
