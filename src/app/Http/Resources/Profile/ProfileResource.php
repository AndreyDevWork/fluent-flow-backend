<?php

namespace App\Http\Resources\Profile;

use App\Http\Authorize\AuthorizeProfile\CanReadProfileResourceHandler;
use App\Http\Authorize\AuthorizeProfile\CanUpdateProfileResourceHandler;
use App\Http\Authorize\AuthorizeProfile\Enums\AuthorizeProfileEnum;
use App\Http\Authorize\PermissionProcessor;
use App\Http\Resources\Image\ImageResource;
use App\Http\Resources\User\UserResource;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes as OA;

#[
    OA\Schema(
        schema: "ProfileResource",
        properties: [
            "date_of_birth" => new OA\Property(
                property: "date_of_birth",
                type: "datetime",
                example: "2001/02/14",
                nullable: true
            ),
            "firstname" => new OA\Property(
                property: "firstname",
                type: "string",
                example: "Slark"
            ),
            "lastname" => new OA\Property(
                property: "lastname",
                type: "string",
                example: "Puchina"
            ),
            "about_me" => new OA\Property(
                property: "about_me",
                type: "text",
                example: "I like English"
            ),
            "role" => new OA\Property(
                property: "role",
                type: "string",
                example: "student"
            ),
            "user" => new OA\Property(
                property: "user",
                ref: "#/components/schemas/UserResource",
                type: "object"
            ),
            "authorize" => new OA\Property(
                property: "authorize",
                description: "authorize profile",
                type: "array",
                items: new OA\Items(type: "string")
            ),
            "avatar" => new OA\Property(
                property: "avatar",
                ref: "#/components/schemas/ImageResource",
                type: "object"
            ),
        ]
    )
]
class ProfileResource extends JsonResource
{
    public static $wrap = false;

    private array $authorize;

    public function __construct($resource)
    {
        parent::__construct($resource);

        $this->authorize = (new PermissionProcessor())
            ->setNext(new CanReadProfileResourceHandler($this->user))
            ->setNext(new CanUpdateProfileResourceHandler($this->user))
            ->process();
    }
    public function toArray(Request $request): array
    {
        return [
            "date_of_birth" => $this->date_of_birth,
            "firstname" => $this->firstname,
            "lastname" => $this->lastname,
            "role" => $this->role->role,
            "user" => new UserResource($this->user),
            "authorize" => $this->authorize,
            "avatar" => new ImageResource($this->avatar),
            "about_me" => $this->about_me,
        ];
    }
}
