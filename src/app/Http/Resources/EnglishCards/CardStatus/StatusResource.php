<?php

namespace App\Http\Resources\EnglishCards\CardStatus;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes as OA;

#[
    OA\Schema(
        schema: "StatusResource",
        properties: [
            "id" => new OA\Property(
                property: "id",
                type: "integer",
                example: 1
            ),
            "status" => new OA\Property(
                property: "status",
                type: "string",
                example: "draft"
            ),
        ]
    )
]
class StatusResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            "status" => $this->status,
        ];
    }
}
