<?php

namespace App\Http\Resources\EnglishCards\Card;

use App\Http\Resources\EnglishCards\CardStatus\StatusResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes as OA;

#[
    OA\Schema(
        schema: "CardResource",
        properties: [
            "id" => new OA\Property(
                property: "id",
                type: "integer",
                example: 1
            ),
            "term" => new OA\Property(
                property: "term",
                type: "string",
                example: "Hello"
            ),
            "translate" => new OA\Property(
                property: "translate",
                type: "string",
                example: "Привет"
            ),
            "status" => new OA\Property(
                property: "status",
                ref: "#/components/schemas/StatusResource",
                type: "object"
            ),
            "author" => new OA\Property(
                property: "author",
                properties: [
                    "firstname" => new OA\Property(
                        property: "firstname",
                        type: "string",
                        example: "Slark"
                    ),
                    "lastname" => new OA\Property(
                        property: "lastname",
                        type: "string",
                        example: "Puchina"
                    ),
                    "user" => new OA\Property(
                        property: "user",
                        ref: "#/components/schemas/UserResource",
                        type: "object"
                    ),
                ],
                type: "object"
            ),
            "authorize" => new OA\Property(
                property: "authorize",
                description: "authorize profile",
                type: "array",
                items: new OA\Items(type: "string")
            ),
        ]
    )
]
class CardResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            "term" => $this->term,
            "status_id" => $this->status_id,
            "translate" => $this->translate,
            "author" => [
                "firstname" => $this->author->firstname,
                "lastname" => $this->author->lastname,
                "user" => new UserResource($this->author->user),
            ],
            "status" => new StatusResource($this->status),
            "authorize" => [],
        ];
    }
}
