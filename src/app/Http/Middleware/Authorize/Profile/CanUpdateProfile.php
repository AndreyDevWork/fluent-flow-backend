<?php

namespace App\Http\Middleware\Authorize\Profile;

use App\Http\Authorize\AuthorizeApp\CanUseProfileHandler;
use App\Http\Authorize\AuthorizeProfile\CanUpdateProfileResourceHandler;
use App\Interfaces\ChainOfResponsibility\Handler;
use App\Models\User;
use App\Traits\HasJsonResponses;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CanUpdateProfile
{
    use HasJsonResponses;

    protected Handler|null $handler = null;
    protected User|null $user;

    public function __construct(protected Request $request)
    {
        $this->user = User::find($this->request->route(param: "user"));

        $this->user &&
            ($this->handler = new CanUpdateProfileResourceHandler($this->user));
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        return $this->handler?->handle()
            ? $next($request)
            : $this->accessDeniedResponse();
    }
}
