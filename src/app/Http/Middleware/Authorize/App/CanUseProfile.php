<?php

namespace App\Http\Middleware\Authorize\App;

use App\Http\Authorize\AuthorizeApp\CanUseProfileHandler;
use App\Interfaces\ChainOfResponsibility\Handler;
use App\Traits\HasJsonResponses;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CanUseProfile
{
    use HasJsonResponses;

    protected Handler $handler;

    public function __construct()
    {
        $this->handler = new CanUseProfileHandler();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        return $this->handler->handle()
            ? $next($request)
            : $this->accessDeniedResponse();
    }
}
