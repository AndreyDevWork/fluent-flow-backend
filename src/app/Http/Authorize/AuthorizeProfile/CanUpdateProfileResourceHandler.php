<?php

namespace App\Http\Authorize\AuthorizeProfile;

use App\Http\Authorize\AbstractResourceHandler;
use App\Http\Authorize\AuthorizeProfile\Enums\AuthorizeProfileEnum;
use App\Interfaces\ChainOfResponsibility\Handler;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class CanUpdateProfileResourceHandler extends AbstractResourceHandler
{
    public function handle(): string|null
    {
        return Auth::id() == $this->model->id
            ? AuthorizeProfileEnum::CAN_UPDATE_PROFILE->name
            : null;
    }
}
