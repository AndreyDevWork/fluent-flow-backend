<?php

namespace App\Http\Authorize\AuthorizeProfile;

use App\Http\Authorize\AbstractResourceHandler;
use App\Http\Authorize\AuthorizeProfile\Enums\AuthorizeProfileEnum;
use App\Interfaces\ChainOfResponsibility\Handler;
use Illuminate\Support\Facades\Auth;

class CanReadProfileResourceHandler extends AbstractResourceHandler
{
    public function handle(): string|null
    {
        return AuthorizeProfileEnum::CAN_READ_PROFILE->name;
    }
}
