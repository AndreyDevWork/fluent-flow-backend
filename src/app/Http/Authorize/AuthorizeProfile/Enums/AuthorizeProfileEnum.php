<?php

namespace App\Http\Authorize\AuthorizeProfile\Enums;

enum AuthorizeProfileEnum
{
    case CAN_READ_PROFILE;
    case CAN_UPDATE_PROFILE;
}
