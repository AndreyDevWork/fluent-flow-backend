<?php

namespace App\Http\Authorize;

use App\Interfaces\ChainOfResponsibility\Handler;
use App\Interfaces\ChainOfResponsibility\NextHandlerSetter;

class PermissionProcessor implements NextHandlerSetter
{
    private $handlers = [];

    public function setNext(Handler $handler): self
    {
        $this->handlers[] = $handler;

        return $this;
    }

    public function process(): array
    {
        $permissions = [];
        foreach ($this->handlers as $handler) {
            $result = $handler->handle();
            $result && ($permissions[] = $result);
        }

        return $permissions;
    }
}
