<?php

namespace App\Http\Authorize;

use App\Interfaces\ChainOfResponsibility\Handler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

abstract class AbstractResourceHandler implements Handler
{
    public function __construct(protected Model $model)
    {
    }
}
