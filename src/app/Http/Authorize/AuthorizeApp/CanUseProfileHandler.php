<?php

namespace App\Http\Authorize\AuthorizeApp;

use App\Http\Authorize\AbstractResourceHandler;
use App\Http\Authorize\AuthorizeApp\Enums\AuthorizeAppEnum;
use App\Interfaces\ChainOfResponsibility\Handler;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class CanUseProfileHandler implements Handler
{
    public function handle(): string|null
    {
        return Auth::id() && User::find(Auth::id())->profile instanceof Profile
            ? AuthorizeAppEnum::CAN_USE_PROFILE->name
            : null;
    }
}
