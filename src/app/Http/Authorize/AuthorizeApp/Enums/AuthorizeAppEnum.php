<?php

namespace App\Http\Authorize\AuthorizeApp\Enums;

enum AuthorizeAppEnum
{
    case CAN_USE_APP;
    case IS_AUTHENTICATED;
    case IS_NOT_AUTHENTICATED;
    case CAN_USE_PROFILE;
    case CAN_CREATE_PROFILE;
}
