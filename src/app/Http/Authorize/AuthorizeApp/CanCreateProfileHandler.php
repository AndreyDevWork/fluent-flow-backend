<?php

namespace App\Http\Authorize\AuthorizeApp;

use App\Http\Authorize\AbstractResourceHandler;
use App\Http\Authorize\AuthorizeApp\Enums\AuthorizeAppEnum;
use App\Interfaces\ChainOfResponsibility\Handler;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class CanCreateProfileHandler implements Handler
{
    public function handle(): string|null
    {
        if (!Auth::id()) {
            return null;
        }

        return User::find(Auth::id())->profile instanceof Profile
            ? null
            : AuthorizeAppEnum::CAN_CREATE_PROFILE->name;
    }
}
