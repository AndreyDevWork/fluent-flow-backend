<?php

namespace App\Http\Authorize\AuthorizeApp;

use App\Http\Authorize\AbstractResourceHandler;
use App\Http\Authorize\AuthorizeApp\Enums\AuthorizeAppEnum;
use App\Interfaces\ChainOfResponsibility\Handler;

class CanUseAppHandler implements Handler
{
    public function handle(): string|null
    {
        return AuthorizeAppEnum::CAN_USE_APP->name;
    }
}
