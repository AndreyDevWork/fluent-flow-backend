<?php

namespace App\Http\Authorize\AuthorizeApp;

use App\Http\Authorize\AbstractResourceHandler;
use App\Http\Authorize\AuthorizeApp\Enums\AuthorizeAppEnum;
use App\Interfaces\ChainOfResponsibility\Handler;
use Illuminate\Support\Facades\Auth;

class IsLoginInHandler implements Handler
{
    public function handle(): string
    {
        return Auth::id()
            ? AuthorizeAppEnum::IS_AUTHENTICATED->name
            : AuthorizeAppEnum::IS_NOT_AUTHENTICATED->name;
    }
}
