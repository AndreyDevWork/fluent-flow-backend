<?php

namespace App\Http\Requests\Image;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Attributes as OA;

#[
    OA\Schema(
        schema: "UploadImageRequest",
        properties: [
            "images" => new OA\Property(
                property: "images[]",
                type: "array",
                items: new OA\Items(
                    description: "Image file to be uploaded",
                    type: "string",
                    format: "binary"
                )
            ),
        ]
    )
]
class UploadImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "images" => "required|array",
            "images.*" => "required|mimes:jpg,png,jpeg,webp|max:102400",
        ];
    }
}
