<?php

namespace App\Http\Requests\Profile;

use App\Http\Requests\Image\UploadImageRequest;
use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Attributes as OA;

#[
    OA\Schema(
        schema: "ProfileStoreRequest",
        required: ["date_of_birth", "firstname", "lastname", "role_id"],
        properties: [
            "date_of_birth" => new OA\Property(
                property: "date_of_birth",
                type: "string",
                example: "2001/02/14"
            ),
            "firstname" => new OA\Property(
                property: "firstname",
                type: "string",
                example: "Slark"
            ),
            "lastname" => new OA\Property(
                property: "lastname",
                type: "string",
                example: "Puchina"
            ),
            "role_id" => new OA\Property(
                property: "role_id",
                type: "integer",
                example: "1"
            ),
            "avatar" => new OA\Property(
                property: "images[]",
                type: "array",
                items: new OA\Items(
                    description: "Avatar of profile image",
                    type: "string",
                    format: "binary"
                )
            ),
            "about_me" => new OA\Property(
                property: "about_me",
                type: "string",
                example: "I like English"
            ),
        ]
    )
]
class ProfileStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "date_of_birth" => "required|date",
            "firstname" =>
                "required|string|max:20|min:2|regex:/^[a-zA-Zа-яА-ЯёЁ\s]+$/u",
            "lastname" =>
                "required|string|max:20|min:2|regex:/^[a-zA-Zа-яА-ЯёЁ\s]+$/u",
            "about_me" => "string|nullable",
            "role_id" => "required|integer",
            "images" => "array",
            "images.*" => "mimes:jpg,png,jpeg,webp|max:102400",
        ];
    }
}
