<?php

namespace App\Http\Requests\Card;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Attributes as OA;

#[
    OA\Schema(
        schema: "CardRequest",
        required: ["status_id"],
        properties: [
            "term" => new OA\Property(
                property: "term",
                type: "string",
                example: "Hello"
            ),
            "translate" => new OA\Property(
                property: "translate",
                type: "string",
                example: "Привет"
            ),
            "status_id" => new OA\Property(
                property: "id",
                type: "integer",
                example: 1
            ),
        ]
    )
]
class CardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "term" => "string",
            "translate" => "string",
            "status_id" => "required|integer|exists:card_statuses,id",
        ];
    }
}
