<?php

namespace App\Http\Controllers\Auth;

use App\Enums\Queue;
use App\Http\Controllers\Controller;
use App\Jobs\Notify\VerifyEmailJob;
use App\Listeners\EmailVerifiedListener;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use OpenApi\Attributes as OA;

#[
    OA\Post(
        path: "/api/auth/send-email-verification-message",
        operationId: "sendEmailVerificationMessage",
        summary: "send email verification message",
        security: [["bearerAuth" => []]],
        tags: ["Authentication"],
        responses: [
            new OA\Response(
                response: 200,
                description: "The user has successfully send email verification message.",
                content: new OA\JsonContent(
                    properties: [
                        "message" => new OA\Property(
                            property: "message",
                            type: "string",
                            example: "Verification email sent"
                        ),
                    ]
                )
            ),
        ]
    )
]
class VerifyEmailController extends Controller
{
    public function sendEmailVerificationMessage(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json(
                data: ["error" => "Email already verified"],
                status: Response::HTTP_CONFLICT
            );
        }

        VerifyEmailJob::dispatch(\Auth::user())->onQueue(Queue::NOTIFY->value);
        return response()->json(
            data: ["message" => "Verification email sent"],
            status: Response::HTTP_ACCEPTED
        );
    }

    public function verificationEmail(Request $request, $id, $token)
    {
        $user = User::find(id: $id);

        if (
            !hash_equals(
                (string) $token,
                sha1(string: $user->getEmailForVerification())
            )
        ) {
            return response()->json(
                data: ["error" => "Invalid verification link"],
                status: Response::HTTP_BAD_REQUEST
            );
        }

        if ($user->hasVerifiedEmail()) {
            return response()->json(
                data: ["error" => "Email already verified"],
                status: Response::HTTP_CONFLICT
            );
        }

        if ($user->markEmailAsVerified()) {
            Cache::forget("profiles:" . $user->id);
            event(event: new Verified($user));
        }

        return Redirect::to(path: env(key: "FRONT_URL"));
    }
}
