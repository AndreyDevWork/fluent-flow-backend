<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Profile\ProfileResource;
use App\Models\Profile;
use App\Traits\HasJsonResponses;
use Illuminate\Support\Facades\Cache;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

#[
    OA\Get(
        path: "/api/profile/{userId}",
        operationId: "getProfile",
        summary: "Get profile by USER ID",
        security: [["bearerAuth" => []]],
        tags: ["Profile"],
        parameters: [
            new OA\Parameter(
                name: "id",
                description: "ID of the user",
                in: "path",
                schema: new OA\Schema(type: "integer", format: "int64")
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: "Profile",
                content: new OA\JsonContent(
                    ref: "#/components/schemas/ProfileResource"
                )
            ),
            new OA\Response(response: 404, description: "Profile not found"),
        ]
    )
]
class ShowProfileController extends Controller
{
    use HasJsonResponses;
    public function __invoke(int $userId)
    {
        $profile = $this->getCache(userId: $userId);

        return $profile instanceof Profile
            ? response()->json(
                data: new ProfileResource(resource: $profile),
                status: Response::HTTP_OK
            )
            : $this->resourceNotFoundResponse();
    }

    private function getCache($userId): Profile|null
    {
        return Cache::remember(
            key: "profiles:$userId",
            ttl: hour(),
            callback: function () use ($userId) {
                return Profile::where(
                    column: "user_id",
                    operator: "=",
                    value: $userId
                )->first();
            }
        );
    }
}
