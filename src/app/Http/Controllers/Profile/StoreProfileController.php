<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\ProfileStoreRequest;
use App\Http\Resources\Profile\ProfileResource;
use App\Models\Profile;
use App\Services\Profile\ProfileStoreService;
use Illuminate\Support\Facades\Cache;
use OpenApi\Attributes as OA;

#[
    OA\Post(
        path: "/api/profile",
        operationId: "createProfile",
        summary: "Create profile of user",
        security: [["bearerAuth" => []]],
        requestBody: new OA\RequestBody(
            content: new OA\MediaType(
                mediaType: "multipart/form-data",
                schema: new OA\Schema(
                    ref: "#/components/schemas/ProfileStoreRequest"
                )
            )
        ),
        tags: ["Profile"],
        responses: [
            new OA\Response(
                response: 201,
                description: "The profile has successfully created profile",
                content: new OA\JsonContent(
                    ref: "#/components/schemas/ProfileResource"
                )
            ),
            new OA\Response(
                response: 409,
                description: "Profile already exists for this user."
            ),
            new OA\Response(response: 422, description: "Validation error"),
        ]
    )
]
class StoreProfileController extends Controller
{
    public function __invoke(
        ProfileStoreRequest $request,
        ProfileStoreService $storeService
    ) {
        $data = $request->validated();
        $profile = $storeService->store(data: $data);
        $this->cache(profile: $profile);

        return $profile instanceof Profile
            ? new ProfileResource(resource: $profile)
            : $profile;
    }

    private function cache($profile): void
    {
        $profile instanceof Profile &&
            Cache::put(
                key: "profiles:" . $profile->user_id,
                value: $profile,
                ttl: hour()
            );
    }
}
