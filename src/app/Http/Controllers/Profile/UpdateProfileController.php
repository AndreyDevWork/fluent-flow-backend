<?php

namespace App\Http\Controllers\Profile;

use App\DTO\ProfilePatchDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\ProfileUpdateRequest;
use App\Http\Resources\Profile\ProfileResource;
use App\Models\Profile;
use App\Models\User;
use App\Services\Profile\ProfileUpdateService;
use Illuminate\Support\Facades\Cache;
use OpenApi\Attributes as OA;

#[
    OA\Patch(
        path: "/api/profile/{userId}",
        operationId: "updateProfile",
        summary: "Update auth profile of user",
        security: [["bearerAuth" => []]],
        requestBody: new OA\RequestBody(
            content: new OA\JsonContent(
                ref: "#/components/schemas/ProfileUpdateRequest"
            )
        ),
        tags: ["Profile"],
        parameters: [
            new OA\Parameter(
                name: "id",
                description: "ID of the user",
                in: "path",
                schema: new OA\Schema(type: "integer", format: "int64")
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: "The profile has successfully updated profile",
                content: new OA\JsonContent(
                    ref: "#/components/schemas/ProfileResource"
                )
            ),
            new OA\Response(response: 422, description: "Validation error"),
        ]
    )
]
class UpdateProfileController extends Controller
{
    public function __invoke(
        ProfileUpdateRequest $request,
        int $userId,
        ProfileUpdateService $profileUpdateService
    ) {
        $profile = $profileUpdateService->update(
            data: new ProfilePatchDTO($request->validated()),
            user: User::find(id: $userId)
        );

        $this->cache(profile: $profile);

        return $profile instanceof Profile
            ? new ProfileResource(resource: $profile)
            : $profile;
    }

    private function cache(Profile $profile): void
    {
        Cache::put(
            key: "profiles:$profile->user_id",
            value: $profile,
            ttl: hour()
        );
    }
}
