<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Services\Profile\AvatarService;
use Auth;
use OpenApi\Attributes as OA;

#[
    OA\Delete(
        path: "/api/profile/avatar",
        operationId: "DeleteAvatar",
        summary: "Delete avatar of profile",
        security: [["bearerAuth" => []]],
        tags: ["Profile"],
        responses: [
            new OA\Response(
                response: 204,
                description: "Avatar deleted successfully"
            ),
        ]
    )
]
class DeleteAvatarController extends Controller
{
    public function __invoke(AvatarService $service)
    {
        Auth::user()->profile->avatar instanceof Image &&
            $service->deleteAvatar(avatar: Auth::user()->profile->avatar);

        return response()->noContent();
    }
}
