<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Image\UploadImageRequest;
use App\Http\Resources\Image\ImageResource;
use App\Models\Image;
use App\Models\Profile;
use App\Services\Profile\AvatarService;
use Auth;
use Illuminate\Support\Facades\Cache;
use OpenApi\Attributes as OA;

#[
    OA\Post(
        path: "/api/profile/avatar",
        operationId: "uploadAvatar",
        summary: "Upload user's avatar",
        security: [["bearerAuth" => []]],
        requestBody: new OA\RequestBody(
            content: new OA\MediaType(
                mediaType: "multipart/form-data",
                schema: new OA\Schema(
                    ref: "#/components/schemas/UploadImageRequest"
                )
            )
        ),
        tags: ["Profile"],
        responses: [
            new OA\Response(
                response: 200,
                description: "Avatar uploaded successfully",
                content: new OA\JsonContent(
                    ref: "#/components/schemas/ImageResource"
                )
            ),
            new OA\Response(response: 422, description: "Validation error"),
        ]
    )
]
class UploadAvatarController extends Controller
{
    public function __invoke(
        UploadImageRequest $request,
        AvatarService $service
    ) {
        $profile = Auth::user()->profile;
        $data = $request->validated();
        $avatar = $service->uploadAvatar(
            avatar: $data["images"][0],
            profile: $profile
        );
        $this->cache(profile: $profile);

        return $avatar instanceof Image ? new ImageResource($avatar) : $avatar;
    }

    private function cache(Profile $profile): void
    {
        Cache::put(
            key: "profiles:$profile->user_id",
            value: $profile,
            ttl: hour()
        );
    }
}
