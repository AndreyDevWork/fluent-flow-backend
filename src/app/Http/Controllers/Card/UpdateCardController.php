<?php

namespace App\Http\Controllers\Card;

use App\Http\Controllers\Controller;
use App\Http\Requests\Card\CardRequest;
use App\Http\Resources\EnglishCards\Card\CardResource;
use App\Models\Card;
use OpenApi\Attributes as OA;

#[
    OA\Post(
        path: "/api/card/{cardId}",
        operationId: "UpdateCardStatus",
        summary: "Update status",
        security: [["bearerAuth" => []]],
        requestBody: new OA\RequestBody(
            content: new OA\JsonContent(ref: "#/components/schemas/CardRequest")
        ),
        tags: ["Card"],
        parameters: [
            new OA\Parameter(
                name: "id",
                description: "ID of the card",
                in: "path",
                schema: new OA\Schema(type: "integer", format: "int64")
            ),
        ],
        responses: [
            new OA\Response(
                response: 201,
                description: "The card has successfully updated",
                content: new OA\JsonContent(
                    ref: "#/components/schemas/CardResource"
                )
            ),
            new OA\Response(response: 422, description: "Validation error"),
        ]
    )
]
class UpdateCardController extends Controller
{
    public function __invoke(CardRequest $request, Card $card)
    {
        $data = $request->validated();
        $card->update($data);

        return $card instanceof Card ? new CardResource($card) : $card;
    }
}
