<?php

namespace App\Http\Controllers\Card;

use App\Http\Controllers\Controller;
use App\Http\Resources\EnglishCards\Card\CardResource;
use App\Models\Card;
use OpenApi\Attributes as OA;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

#[
    OA\Get(
        path: "/api/card",
        operationId: "getCards",
        summary: "Get cards",
        security: [["bearerAuth" => []]],
        tags: ["Card"],
        parameters: [
            new OA\Parameter(
                name: "filter[user_id]",
                description: "Filter by user ID",
                in: "query",
                required: false,
                schema: new OA\Schema(type: "integer")
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: "Cards",
                content: new OA\JsonContent(
                    properties: [
                        "data" => new OA\Property(
                            property: "data",
                            type: "array",
                            items: new OA\Items(
                                ref: "#/components/schemas/CardResource"
                            )
                        ),
                    ]
                )
            ),
        ]
    )
]
class IndexCardController extends Controller
{
    public function __invoke()
    {
        $cards = QueryBuilder::for(Card::class)
            ->select("cards.*")
            ->join("profiles", "profiles.id", "=", "cards.profile_id")
            ->allowedFilters([
                AllowedFilter::exact("user_id", "profiles.user_id"),
            ])
            ->orderBy(column: "cards.id", direction: "desc")
            ->cursorPaginate(perPage: 18);

        return CardResource::collection($cards);
    }
}
