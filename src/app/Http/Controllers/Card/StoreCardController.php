<?php

namespace App\Http\Controllers\Card;

use App\Http\Controllers\Controller;
use App\Http\Requests\Card\CardRequest;
use App\Http\Resources\EnglishCards\Card\CardResource;
use App\Models\Card;
use App\Services\Card\StoreCardService;
use Illuminate\Support\Facades\Auth;
use OpenApi\Attributes as OA;

#[
    OA\Patch(
        path: "/api/card",
        operationId: "createCard",
        summary: "Create status",
        security: [["bearerAuth" => []]],
        requestBody: new OA\RequestBody(
            content: new OA\JsonContent(ref: "#/components/schemas/CardRequest")
        ),
        tags: ["Card"],
        responses: [
            new OA\Response(
                response: 201,
                description: "The card has successfully created",
                content: new OA\JsonContent(
                    ref: "#/components/schemas/CardResource"
                )
            ),
            new OA\Response(response: 422, description: "Validation error"),
        ]
    )
]
class StoreCardController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(
        CardRequest $request,
        StoreCardService $storeCardService
    ) {
        $data = $request->validated();
        $card = $storeCardService->store(
            profile: Auth::user()->profile,
            data: $data
        );

        return $card instanceof Card ? new CardResource($card) : $card;
    }
}
