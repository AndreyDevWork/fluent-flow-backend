<?php

namespace App\Http\Controllers\Card;

use App\Http\Controllers\Controller;
use App\Http\Resources\EnglishCards\Card\CardResource;
use App\Models\Card;
use App\Traits\HasJsonResponses;
use OpenApi\Attributes as OA;

#[
    OA\Get(
        path: "/api/card/{cardId}",
        operationId: "getCard",
        summary: "Get card by card ID",
        security: [["bearerAuth" => []]],
        tags: ["Card"],
        parameters: [
            new OA\Parameter(
                name: "id",
                description: "ID of the card",
                in: "path",
                schema: new OA\Schema(type: "integer", format: "int64")
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: "Card",
                content: new OA\JsonContent(
                    ref: "#/components/schemas/CardResource"
                )
            ),
            new OA\Response(response: 404, description: "Card not found"),
        ]
    )
]
class ShowCardController extends Controller
{
    use HasJsonResponses;
    /**
     * Handle the incoming request.
     */
    public function __invoke(Card $card)
    {
        return new CardResource($card);
    }
}
