<?php

namespace App\Http\Controllers\Authorize;

use App\Http\Authorize\AuthorizeApp\CanCreateProfileHandler;
use App\Http\Authorize\AuthorizeApp\CanUseAppHandler;
use App\Http\Authorize\AuthorizeApp\CanUseProfileHandler;
use App\Http\Authorize\AuthorizeApp\IsLoginInHandler;
use App\Http\Authorize\PermissionProcessor;
use App\Http\Controllers\Controller;
use OpenApi\Attributes as OA;

#[
    OA\Get(
        path: "/api/get-authorize-app",
        operationId: "getAuthorizeApp",
        summary: "Get authorize app scopes",
        security: [["bearerAuth" => []]],
        tags: ["Authorize"],
        responses: [
            new OA\Response(
                response: 200,
                description: "Profile",
                content: new OA\JsonContent(
                    type: "array",
                    items: new OA\Items(type: "string")
                )
            ),
        ]
    )
]
class GetAuthorizeAppController extends Controller
{
    public function __construct(private PermissionProcessor $processor)
    {
    }

    public function __invoke(): array
    {
        return $this->processor
            ->setNext(handler: new CanUseAppHandler())
            ->setNext(handler: new CanUseProfileHandler())
            ->setNext(handler: new IsLoginInHandler())
            ->setNext(handler: new CanCreateProfileHandler())
            ->process();
    }
}
