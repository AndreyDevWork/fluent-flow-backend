<?php

namespace App\Listeners;

use App\Enums\Queue;
use App\Jobs\Notify\VerifiedEmailJob;
use Illuminate\Auth\Events\Verified;
use Mail;

class EmailVerifiedListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(Verified $event): void
    {
        VerifiedEmailJob::dispatch($event->user)->onQueue(Queue::NOTIFY->value);
    }
}
