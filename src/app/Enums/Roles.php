<?php

namespace App\Enums;

enum Roles: string
{
    case STUDENT = "student";
    case SPEAKER = "speaker";
}
