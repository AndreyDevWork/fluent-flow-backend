<?php

namespace App\Enums;

enum CardStatuses: string
{
    case DRAFT = "draft";
    case PUBLISHED = "published";
}
