<?php

namespace Tests\Unit;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class GeneratePathFileTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testGeneratePathFile(): void
    {
        $now = Carbon::now();
        $firstDirectory = "superPublic";
        $expectedPath = "$firstDirectory/$now->year/$now->month/$now->day";
        $actualPath = generatePathFile(
            firstCatalog: $firstDirectory,
            now: $now
        );

        $this->assertSame(expected: $expectedPath, actual: $actualPath);
    }
}
