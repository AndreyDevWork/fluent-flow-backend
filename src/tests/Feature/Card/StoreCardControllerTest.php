<?php

namespace Card;

use App\Http\Resources\Profile\ProfileResource;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class StoreCardControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testStore(): void
    {
        $this->actingAs(user: User::find(id: 2), guard: "api");
        $response = $this->post(
            uri: "/api/card",
            data: [
                "term" => "hello",
                "translate" => "привет",
                "status_id" => 2,
            ]
        );

        $response->assertStatus(status: 201);
    }
}
