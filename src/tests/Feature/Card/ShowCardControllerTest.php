<?php

namespace Card;

use App\Http\Resources\Profile\ProfileResource;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class ShowCardControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testStore(): void
    {
        $user = User::find(id: 2);
        $card = $user->profile->cards()->create([
            "term" => "hello",
            "translate" => "привет",
            "status_id" => 2,
        ]);
        $this->actingAs(user: User::find(id: $user->id), guard: "api");
        $response = $this->get(uri: "/api/card/$card->id");

        $response->assertStatus(status: 200);
    }
}
