<?php

namespace Card;

use App\Models\User;
use Tests\TestCase;

class IndexCardControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testStore(): void
    {
        $user = User::find(id: 2);

        $this->actingAs(user: User::find(id: $user->id), guard: "api");
        $response = $this->get(uri: "/api/card");

        $response->assertStatus(status: 200);
    }
}
