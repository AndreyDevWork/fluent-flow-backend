<?php

namespace Auth;

use Tests\TestCase;

class RegisterControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testRegister(): void
    {
        $response = $this->post(
            uri: "/api/auth/register",
            data: [
                "username" => "Slark",
                "password" => "1246qwrt",
            ]
        );

        $response->assertStatus(status: 201);
    }
}
