<?php

namespace Authorize;

use Tests\TestCase;

class GetAuthorizeAppControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testGetAuthorizeApp(): void
    {
        $response = $this->get(uri: "/api/get-authorize-app");

        $response->assertStatus(status: 200);
    }
}
