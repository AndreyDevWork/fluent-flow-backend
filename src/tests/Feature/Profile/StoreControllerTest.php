<?php

namespace Profile;

use App\Http\Resources\Profile\ProfileResource;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class StoreControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testStore(): void
    {
        User::factory()->create(
            attributes: [
                "id" => 200,
            ]
        );
        $this->actingAs(user: User::find(id: 200), guard: "api");
        $response = $this->post(
            uri: "/api/profile",
            data: [
                "lastname" => "New lastname",
                "firstname" => "New firstname",
                "date_of_birth" => "2001/01/15",
                "role_id" => 1,
            ]
        );

        $response->assertStatus(status: 201);
    }
}
