<?php

namespace Profile;

use App\Models\User;
use Tests\TestCase;

class UpdateControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testUpdate(): void
    {
        $this->actingAs(user: User::find(id: 2), guard: "api");
        $response = $this->patch(
            uri: "/api/profile/2",
            data: [
                "lastname" => "New lastname",
            ]
        );

        $response->assertStatus(status: 200);
    }
}
