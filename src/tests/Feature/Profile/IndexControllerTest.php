<?php

namespace Profile;

use App\Models\User;
use Tests\TestCase;

class IndexControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testIndex(): void
    {
        $this->actingAs(user: User::find(id: 2), guard: "api");
        $response = $this->get(uri: "/api/profile");

        $response->assertStatus(status: 200);
    }
}
