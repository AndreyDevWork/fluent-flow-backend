<?php

namespace Profile;

use App\Models\User;
use Tests\TestCase;

class ShowControllerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testShow(): void
    {
        $this->actingAs(user: User::find(id: 2), guard: "api");
        $response = $this->get(uri: "/api/profile/2");

        $response->assertStatus(status: 200);
    }
}
